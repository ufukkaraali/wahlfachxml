package xmlProject;

import java.io.File;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

public class ProjectTab extends Tab {

	TabPane tabPane = new TabPane();
	AnchorPane pane = new AnchorPane();
	TreeTab treeTab;
	CodeTab codeTab;
	Transformer transformer;

	public ProjectTab(Controller c, String text) {
		super(text);
		pane.getChildren().add(tabPane);
		AnchorPane.setBottomAnchor(tabPane, 0.0);
		AnchorPane.setTopAnchor(tabPane, 0.0);
		AnchorPane.setRightAnchor(tabPane, 0.0);
		AnchorPane.setLeftAnchor(tabPane, 0.0);

		// Configuring the tabPane
		tabPane.setSide(Side.BOTTOM);
		tabPane.setPadding(new Insets(0, 0, 0, 0));
		tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

		// Adding TreeTab and CodeTab
		treeTab = new TreeTab(c);
		codeTab = new CodeTab(c);

		tabPane.getTabs().addAll(treeTab, codeTab);
		this.setContent(tabPane);

		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			if (newTab == codeTab) {
				try {
					StringWriter sw = new StringWriter();
					TransformerFactory tf = TransformerFactory.newInstance();
					Transformer transformer = tf.newTransformer();
					transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					transformer.setOutputProperty(OutputKeys.METHOD, "xml");
					transformer.setOutputProperty(OutputKeys.INDENT, "yes");
					transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
					transformer.transform(new DOMSource(treeTab.getXmlBuilder().getRoot()), new StreamResult(sw));
					codeTab.setDisplayedText(sw);
					c.nodeContentTableView.setEditable(false);
					
				} catch (Exception ex) {
					throw new RuntimeException("Error converting to Code", ex);
				}
			}
		});
		
		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			if (newTab == treeTab) {
				c.nodeContentTableView.setEditable(true);
			}
		});
	}

	public ProjectTab(Controller c, String text, File file) {
		this(c, text);
		treeTab.getXmlBuilder().readFile(file);
	}

	public ProjectTab(Controller c, String text, File file, File schema) {
		this(c, text);
		treeTab.getXmlBuilder().readFile(file);
		treeTab.getXmlBuilder().setSchema(schema);
	}

}
