package xmlProject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TableViewContent {

	private ObservableList<Attribute> rows;

	public TableViewContent() {
		rows = FXCollections.observableArrayList();
	}

	public void addRow(String attr, String value) {
		Attribute a = new Attribute(attr, value);
		rows.add(a);
	}

	public void setRow(int row, String attr, String value) throws Exception {
		setAttribute(row, attr);
		setValue(row, value);
	}

	public void setAttribute(int row, String attr) throws DuplicateException {
		// TODO Attribute pops up in random table row
		for(int i = 0; i<rows.size(); i++) {
			if(i != row && rows.get(i).getAttribute().equals(attr)) {
				setAttribute(row, "attr");
				throw new DuplicateException();
			}
		}
		Attribute a = rows.get(row);
		a.setAttribute(attr);
		rows.set(row, a);
	}

	public void setValue(int row, String value) {
		rows.get(row).setValue(value);
		Attribute a = rows.get(row);
		a.setValue(value);
		rows.set(row, a);
	}

	public void deleteRow(int index) {
		rows.remove(index);
	}

	public void deleteAll() {
		rows.remove(0, rows.size() - 1);
	}

	public ObservableList<Attribute> getRows() {
		return rows;
	}

}
