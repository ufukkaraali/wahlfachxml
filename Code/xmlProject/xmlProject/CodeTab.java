package xmlProject;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;

public class CodeTab extends Tab {

    private Controller mainWindowController;
    
    Tab codeTab;
    ScrollPane scrollPane;
    TextArea displayedText;

    
    // Creating a empty CodeTab
    public CodeTab(Controller c){
        // Create new empty XML-File
        super("Codeansicht");
        mainWindowController = c;
        setOnClosed(event -> {
            mainWindowController.emptyTable();
        });
        
        System.out.println("new CodeTab");
        codeTab = new Tab("Codeansicht");
    	displayedText = new TextArea();
    	displayedText.setEditable(isDisabled());
        // Add Scrollbar to Code Tab
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setPannable(true);
        scrollPane.setContent(displayedText);
        
        // Set Anchors
        AnchorPane.setTopAnchor(scrollPane, 0.0);
        AnchorPane.setRightAnchor(scrollPane, 0.0);
        AnchorPane.setBottomAnchor(scrollPane, 0.0);
        AnchorPane.setLeftAnchor(scrollPane, 0.0);
        this.setContent(scrollPane);
    }
    
    public void setDisplayedText(StringWriter text) {
    	String newtext = text.toString();
    	//newtext.replaceAll(">", ">\r\n");
    	this.displayedText.setText(newtext);
    }
    
    /*
    // Aufruf einer vorhandenen Datei
    public CodeTab(Controller c, String text, File file){
    	this(c, text);
    	System.out.println(file.getAbsolutePath());

        displayedtext.setFont(Font.getDefault());
        displayedtext.getLayoutBounds();
        codeContent.getChildren().add(displayedtext);
    }
    */

}
