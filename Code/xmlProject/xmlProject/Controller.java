package xmlProject;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import javafx.util.Callback;
import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

public class Controller{

    @FXML private TreeView<String> projectTreeView;
    @FXML private MenuItem openProjectMenu;
    @FXML private MenuItem openFileMenu;
    @FXML private MenuItem closeMenu;

    @FXML private TabPane tabPane;
    @FXML private VBox rightSidePanel;

    @FXML TableView<Attribute> nodeContentTableView;
    @FXML private TableColumn<Attribute, String> tableColumnAttribute;
    @FXML private TableColumn<Attribute, String> tableColumnValue;

    @FXML private Button addAttributeButton;
    //@FXML private Button editAttributeButton;
    @FXML private Button deleteAttributeButton;

    //private EventHandler<TableColumn.CellEditEvent<Attribute, String>> handleEditCell;

    public DraggableNode focusedNode;
    //public DraggableNode copyElement;

    public Controller(){

    }

    @FXML public void initialize() {

        tableColumnAttribute.setPrefWidth(100);

        tableColumnValue.prefWidthProperty().bind(
                nodeContentTableView.widthProperty().subtract(
                        tableColumnAttribute.widthProperty().subtract(2)
                )
        );

        /*handleEditCell = new EventHandler<TableColumn.CellEditEvent<Attribute, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Attribute, String> t) {
                ((Attribute) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                ).setAttribute(t.getNewValue());
            }
        };*/

        tableColumnAttribute.setCellValueFactory(new PropertyValueFactory<Attribute,String>("Attribute"));
        tableColumnValue.setCellValueFactory(new PropertyValueFactory<Attribute,String>("Value"));

        Callback<TableColumn<Attribute, String>, TableCell<Attribute, String>> cellFactory =
                new Callback<TableColumn<Attribute, String>, TableCell<Attribute, String>>() {
                    public TableCell<Attribute, String> call(TableColumn<Attribute, String> p) {
                        return new EditCell();
                    }
                };

        tableColumnAttribute.setCellFactory(TextFieldTableCell.forTableColumn());
        tableColumnAttribute.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Attribute, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Attribute, String> t) {
                        Attribute newattribute = t.getTableView().getItems().get(
                                t.getTablePosition().getRow());

                        newattribute.setAttribute(t.getNewValue());
                        System.out.println("" + t.getNewValue());
                        try {
							focusedNode.tableViewContent.setAttribute(t.getTablePosition().getRow(), t.getNewValue());
	                        focusedNode.linkedAttributes.set(t.getTablePosition().getRow(), newattribute);
	                        focusedNode.tableViewContent.setValue(t.getTablePosition().getRow(), newattribute.getValue());
	                        focusedNode.refreshAttributes();
						} catch (DuplicateException e) {
							Alert alert = new Alert(AlertType.ERROR);
	                        alert.setTitle("Fehler");
	                        alert.setHeaderText("Duplikat erkannt");
	                        alert.setContentText("Es kann keine zwei Attribute mit demselben Namen in einem Knoten geben.");
	                        alert.showAndWait();
						}
                    }
                }
        );

        tableColumnValue.setCellFactory(TextFieldTableCell.forTableColumn());
        tableColumnValue.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Attribute, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Attribute, String> t) {
                        Attribute a = t.getTableView().getItems().get(
                                t.getTablePosition().getRow());
                        ((Attribute) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setValue(t.getNewValue());

                        focusedNode.tableViewContent.setValue(t.getTablePosition().getRow(), t.getNewValue());
                        focusedNode.linkedAttributes.set(t.getTablePosition().getRow(), a);
                        focusedNode.refreshAttributes();
                    }
                }    
        );

        Image image = new Image(getClass().getResourceAsStream("/Images/addButton1.png"));
        addAttributeButton.setGraphic(new ImageView(image));
        
        // Edit Button not in use at the moment
        //Image image1 = new Image(getClass().getResourceAsStream("/Images/editButton1.png"));
        //editAttributeButton.setGraphic(new ImageView(image1));
        
        Image image2 = new Image(getClass().getResourceAsStream("/Images/deleteButton1.png"));
        deleteAttributeButton.setGraphic(new ImageView(image2));

    }

    //Ordnersymbol der Projekts
    /*private final Node rootProjectImg = new ImageView(
            new Image(getClass().getResourceAsStream(""))
    );*/
    
    /**
     * Nach dem Ausw�hlen des Men�punkts Datei->Neu->XML wird ein neues Fenster zur Bearbeitung
     * der XML ge�ffnet, sowie das Projektverzeichnis auf der linken Seite aktualisiert
     */
    public void handleNewProject() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("newFileDialog.fxml"));
            //fxmlLoader.setLocation();

            Scene scene = new Scene(fxmlLoader.load(), 405, 148);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = new Stage();
            stage.setTitle("Neue XML-Datei");
            stage.initStyle(StageStyle.UTILITY);
            stage.setResizable(false);
            stage.setScene(scene);
            stage.show();

            NewFileDialog dialogController = fxmlLoader.getController();

            stage.setOnHidden(event -> {
                String fileText = dialogController.getFileText();

                if(!dialogController.isWindowCancelled()) {

                    if(!fileText.endsWith(".xml")) fileText +=".xml";
                    ProjectTab ProjectTab;
                    DraggableNode root;

                    // Einbinden einer Schema aktiviert und Datei ausgew�hlt -> Schema validieren
                    if(dialogController.isCheckBoxEnabled() &&
                            !dialogController.getSchemaFilePath().isEmpty()) {
                        System.out.println("New File with schematic");
                        File schema = new File(dialogController.getSchemaFilePath());
                        ProjectTab = new ProjectTab(Controller.this, fileText, schema);
                        root = new DraggableNode(this, ProjectTab.treeTab.getXmlBuilder(), dialogController.getRootTextField());
                        //File schema = new File(dialogController.getSchemaFilePath());
                        //tab = new CustomTab(Controller.this, fileText);
                        ProjectTab.treeTab.setSchema(schema);
                    }
                    
                    // Ohne Schema
                    else {
                        System.out.println("New File");
                        ProjectTab = new ProjectTab(Controller.this, fileText);
                        root = new DraggableNode(this, ProjectTab.treeTab.getXmlBuilder(), dialogController.getRootTextField());
                    }

                    root.setLabel(dialogController.getRootTextField());
                    root.tabContent = ProjectTab.treeTab.treeContent;
                    root.setRoot();
                    ProjectTab.treeTab.getXmlBuilder().setRoot(root.getElement());
                    // TODO Add course opener to tag to XML?
                    
                    ProjectTab.treeTab.treeContent.getChildren().add(root);

                    ProjectTab.setClosable(true);
                    tabPane.getTabs().addAll(ProjectTab);
                }

                if(event.getSource() instanceof Button) {
                    Button button = (Button) event.getSource();
                    if(button.isCancelButton()) {
                        System.out.println("Cancel");
                    }
                }

            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Nach dem Ausw�hlen des Menpunkts Datei->�ffnen wird ein Dialog zum W�hlen eines
     * Verzeichnisses ge�ffnet, worauf dann beim W�hlen eines Projektordners das Projektverzeichnis
     * auf der linken Seite aktualisiert wird.
     */
    public void handleOpenProject() {
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File(System.getProperty("user.home")));
        File choice = dc.showDialog(openProjectMenu.getParentPopup().getScene().getWindow());
        if(choice == null || ! choice.isDirectory()) {

        } else {
            projectTreeView.setRoot(getNodesForDirectory(choice));
            //CustomTab tab = new CustomTab();

        }

    }

    public void handleOpenFile() {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home")));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML-File .xml" , "*.xml"));
        File choice = fc.showOpenDialog(openFileMenu.getParentPopup().getScene().getWindow());

        if(choice != null) {
            ProjectTab ProjectTab = new ProjectTab(Controller.this, choice.getName(), choice);
            XMLBuilder builder = ProjectTab.treeTab.getXmlBuilder();
            Element root = builder.readFile(choice);
     
            tabPane.getTabs().addAll(ProjectTab);
            //tabPane.get
            ProjectTab.treeTab.showXML(root);
        }
    }

    private ProjectTab getOpenedTab() {
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        ProjectTab tab = null;
        Tab openendTab = selectionModel.getSelectedItem();
        if(openendTab instanceof ProjectTab) {
            tab = (ProjectTab) openendTab;
        }
        return tab;
    }

    public void handleSave() {
        ProjectTab tab = getOpenedTab();
        if(tab !=null) {
            XMLBuilder b = tab.treeTab.getXmlBuilder();
            String s =b.saveFile(getOpenedTab().getText(), openFileMenu.getParentPopup().getScene().getWindow());
            if(!s.equals(tab.getText()))
                tab.setText(s);
        }

    }

    public void handleSaveAs(){
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Speichern unter");
        ProjectTab tab = getOpenedTab();
        if(tab!=null) {
            File originalFile = getOpenedTab().treeTab.getXmlBuilder().getXmlFile();
            // Es wurde noch nicht gespeichert
            if(originalFile == null) {
                handleSave();
                return;
            }
            File dir = (originalFile.getParentFile());
            chooser.setInitialDirectory(dir);

            chooser.setInitialFileName(tab.getText());
            chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML-File .xml" , "*.xml"));
            File choice = chooser.showSaveDialog(openProjectMenu.getParentPopup().getScene().getWindow());
            if (choice != null) {
                tab.treeTab.getXmlBuilder().saveFileAs(choice);
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Speichern fehlgeschlagen");
            alert.setContentText("Es ist keine Datei zum Speichern ge�ffnet.");

            alert.showAndWait();
        }

    }

    public void handleMouseClicked() {
        rightSidePanel.setOnMouseClicked((event) -> {
            if(event.getButton() == MouseButton.SECONDARY) {
                ContextMenu menu = new ContextMenu();
                /*MenuItem item = new MenuItem("Neuer Knoten");
                item.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        DraggableNode node = new DraggableNode(Controller.this, getOpenedTab().treeTab.getXmlBuilder(), "Label");

                        rightSidePanel.getChildren().add(node);
                    }
                }); */
                MenuItem chapter = new MenuItem("chapter");
                chapter.setOnAction(event1 -> {
                    DraggableNode node = new DraggableNode(Controller.this, getOpenedTab().treeTab.getXmlBuilder(), "chapter");
                    // TODO Dynamic numbering of chapters
                    node.tableViewContent.addRow("nr","0");
                    rightSidePanel.getChildren().add(node);
                });
                MenuItem page = new MenuItem("page");
                page.setOnAction(event1 -> {
                    DraggableNode node = new DraggableNode(Controller.this, getOpenedTab().treeTab.getXmlBuilder(), "page");
                    node.tableViewContent.addRow("title","");
                    rightSidePanel.getChildren().add(node);
                });
                MenuItem content = new MenuItem("content");
                content.setOnAction(event1 -> {
                    DraggableNode node = new DraggableNode(Controller.this, getOpenedTab().treeTab.getXmlBuilder(), "content");
                    node.tableViewContent.addRow("type","0");
                    rightSidePanel.getChildren().add(node);
                });

                menu.getItems().addAll(chapter, page, content);
                menu.show(rightSidePanel, event.getScreenX(), event.getSceneY());

            }});

    }

    public void showTable() {
        ObservableList<Attribute> list =  focusedNode.tableViewContent.getRows();
        ObservableList<Attribute> items = nodeContentTableView.getItems();
        if(list.size()==0) {
            nodeContentTableView.setPlaceholder(new Label("Keine Attribute"));
        }
        nodeContentTableView.setItems(list);

    }
    
    public void emptyTable() {
        nodeContentTableView.getItems().removeAll(nodeContentTableView.getItems());
    }
    
    //Returns a TreeItem representation of the specified directory
    public TreeItem<String> getNodesForDirectory(File directory) { 
        TreeItem<String> root = new TreeItem<String>(directory.getName());
        for(File f : directory.listFiles()) {
            System.out.println("Loading " + f.getName());
            if(f.isDirectory()) { //Then we call the function recursively
                root.getChildren().add(getNodesForDirectory(f));
            } else {
                root.getChildren().add(new TreeItem<String>(f.getName()));
            }
        }
        return root;
    }
    
    // Refreshes the TreeTab-View after a Drag&Drop-Actin
    public void refreshProject(Element root) {
    	// Get currently opened ProjectTab
    	ProjectTab currenttab = (ProjectTab) tabPane.getSelectionModel().getSelectedItem();
    	// Refresh TreeTab in currently opened Project
    	currenttab.treeTab.refresh(root);
    }

    public void handleStartAdler() {

        File adlersource = new File(System.getProperty("user.dir") + "//adler_v1b.jar");
        System.out.println(System.getProperty("user.dir"));
        //Select one xml file to be transformed into HTMLs.
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home")));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML-File .xml" , "*.xml"));
        File xmlfile = fc.showOpenDialog(openFileMenu.getParentPopup().getScene().getWindow());
        
        // Copying a temporary version of adler.jar into the directory of the xml file
        if(xmlfile != null) {
        	File adlerdest = new File(xmlfile.getParent() + "\\adler_temp.jar");
        	try {
        		if(adlersource.exists() && !adlerdest.exists()) {
        			Files.copy(adlersource.toPath(), adlerdest.toPath());
        		}
			} catch (IOException e1) {
				System.out.println("Copying failed");
				e1.printStackTrace();
			}
        
        // Execute Adler to create HTML files
        try {
            System.out.println("Starting Adler with file: " + xmlfile.getAbsolutePath());
        	ProcessBuilder pb = new ProcessBuilder("java", "-jar", adlerdest.getAbsolutePath(), xmlfile.getAbsolutePath());
        	Process process = pb.start();
        	BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        	String s = "";
        	while((s = in.readLine()) != null){
        	    System.out.println(s);
        	}
        	int status = process.waitFor();
        	System.out.println("Exited with status: " + status);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //Delete temporary adler.jar
        adlerdest.delete();
        
        //Give alert to report success
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Noch nicht verf�gbar");
        alert.setContentText("AdLeR kann in dieser Version noch nicht ausgef�hrt werden.");

        alert.showAndWait();
        /*try {
            File file = new File("adler/adler_v1.jar");
            Runtime.getRuntime().exec("java", new String[] {"-jar"}, file);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        }
    }

    public void handleAddAttribute(ActionEvent actionEvent) {
        if(focusedNode != null) {
        	Attribute newatt = new Attribute("attr", "val");
            if (focusedNode.linkedAttributes.contains(newatt)) {
            	Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Es existiert bereits ein neues Attribut");
                alert.setContentText("Es kann maximal ein neues Attribut auf einmal angelegt werden.");
            }
            focusedNode.tableViewContent.addRow("attr", "val");
            focusedNode.linkedAttributes.add(newatt);
            focusedNode.refreshAttributes();
            nodeContentTableView.setItems(focusedNode.tableViewContent.getRows());
        }
    }
    
    public void handleDeleteAttribute(ActionEvent actionEvent) {
        if(focusedNode != null) {
            Attribute a = nodeContentTableView.getSelectionModel().getSelectedItem();
            int index = nodeContentTableView.getSelectionModel().getFocusedIndex();
            focusedNode.tableViewContent.deleteRow(index);
            focusedNode.linkedAttributes.remove(index);
            focusedNode.refreshAttributes();
            nodeContentTableView.getItems().remove(a);
        }
    }

    public void handleDeleteElement() {
        if(focusedNode != null && !focusedNode.isRoot) {
            focusedNode.deleteSelf();
        }
    }

    public void handleClose() {
        Stage stage = (Stage) addAttributeButton.getScene().getWindow();
        stage.close();
    }

    /*
    public void handleCopyElement() {
        if(focusedNode!=null)
            copyElement = focusedNode;
    }

    public DraggableNode getCopyElement() {
        return copyElement;
    }*/
    
}
